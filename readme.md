[![build-status](https://bitbucket-badges.useast.atlassian.io/badge/brooedv/bbqmqtt.svg)](https://bitbucket.org/brooedv/bbqmqtt/addon/pipelines/home)

# Test-Ballon für Arduino<->CC3000<->Broker<->JavaClient-Kommunikation über MQTT #

### Resources ###
Moquette MQTT-Java-Broker

* https://github.com/andsel/moquette
* https://projects.eclipse.org/proposals/moquette-mqtt

PAHO MQTT-Java-Client

* https://www.eclipse.org/paho/clients/java/
* http://www.infoq.com/articles/practical-mqtt-with-paho

PupSubClient MQTT-Arduino-Client (CC3000-convert) 

* https://twitter.com/aceone_/status/502373483314413568
* https://github.com/allthingstalk/arduino-client/wiki/tutorial