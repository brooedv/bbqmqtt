
#include <ccspi.h>
#include <SPI.h>
#include <Adafruit_CC3000.h>
#include <PubSubClient.h>

//WiFi
#define ADAFRUIT_CC3000_IRQ   3
#define ADAFRUIT_CC3000_VBAT  2
#define ADAFRUIT_CC3000_CS    4
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT, SPI_CLOCK_DIVIDER);

#define WLAN_SSID       "<SSID>"
#define WLAN_PASS       "<PASS>"
#define WLAN_SECURITY   WLAN_SEC_WPA2

//MQTT
byte server[] = { 192, 168, 0, 155 };
PubSubClient client(server, 1883, callback, cc3000);

void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
  Serial.print("> ");
  Serial.print(topic);
  Serial.print(" : ");

  String msgString; 
  {                                                     
    char message_buff[length + 1];
    strncpy(message_buff, (char*)payload, length);
    message_buff[length] = '\0';
    msgString = String(message_buff);
  }  
  
  Serial.println(msgString);
}

// Counter
int count=0;
char temp[20];


void setup() {
  Serial.begin(9600);

  setupWLAN();
  
  if (client.connect("arduinoClient")){
    Serial.println("started");
    client.publish("arduino/out/status","started");
    client.subscribe("arduino/in/#");
  }

}

void loop() {

  sprintf(temp,"%d",count);
  Serial.println(temp);
  client.publish("arduino/out/count",temp);
  count++;
  client.loop();
  delay(500);

}

void setupWLAN(){
  cc3000.begin();

  if (!cc3000.deleteProfiles()) {
    while(1);
  }

  if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
    while(1);
  }
   
  while (!cc3000.checkDHCP())
  {
    delay(100); // ToDo: Insert a DHCP timeout!
  }  
}
