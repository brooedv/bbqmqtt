package com.brooedv.mqtt.test;

import org.dna.mqtt.moquette.server.Server;
import org.eclipse.paho.client.mqttv3.*;

import java.io.IOException;

/**
 * Created by berni on 27.03.15.
 */
public class MQTTTestBroker {

    public static void main(String[] args) throws IOException, MqttException {
        startServer();
        addGlobalSubscriber();
    }

    private static void startServer() throws IOException {
        final Server server = new Server();
        server.startServer();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                server.stopServer();
            }
        });
    }

    private static void addGlobalSubscriber() throws MqttException{
        final String clientID = "GlobalSubscriber";

        MqttClientPersistence persistence = null;
        MqttClient globalSubscriber = new MqttClient("tcp://localhost:1883", clientID, persistence);

        globalSubscriber.setCallback(new MqttCallback() {

            public void connectionLost(Throwable throwable) {
                throwable.printStackTrace();
            }


            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                System.out.println("<"+topic + ">"+mqttMessage.getQos()+":" + mqttMessage);
            }


            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                //System.out.println(iMqttDeliveryToken);
            }
        });

        globalSubscriber.connect();
        globalSubscriber.subscribe("#");

    }

}
