package com.brooedv.mqtt.test;

import org.eclipse.paho.client.mqttv3.*;

/**
 * Created by berni on 26.03.15.
 */
public class MQTTTestClient {

    public static void main(String[] args){
        try {
            MqttClientPersistence persistence = null;
            MqttClient client = new MqttClient("tcp://localhost:1883", "pub2", persistence);
            client.connect();

            MqttMessage message = new MqttMessage();
            message.setPayload("Huhu".getBytes());
            client.publish("arduino/in/test", message);
            client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
